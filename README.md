### General Info
***
ECF-LODECOM : projet développé pour la société LODECOM.
Par Simplon.Co DWWM Castres 2021.
## Technologies
***
A list of technologies used within the project:
* [Vue js](https://vuejs.org): Version 2.6.12 
* [Twig](https://twig.symfony.com/doc/3.x/): Version 2.0
* [bootstrap](https://getbootstrap.com/docs/4.0/): Version 4.6.0
* [jquery](https://code.jquery.com/jquery-3.5.1.slim.min.js): Version 3.5.1
* [MySQL](https://www.mysql.com/fr/): Ver 14.14 Distrib 5.7.33
* [html](https://www.w3schools.com/): Version 5
* [css](https://www.w3schools.com/): Version 3
* [php](https://getbootstrap.com/docs/4.0/): Version 7.2.24
## Installation
***
Le projet LODECOM utilise une base de donnée.
  1 - charger le ficier db_dump.sql.
  2 - donner les droits d'accés à l'utilisateur de la base de donnée lodecom_sample
  3 - dans le fichier config_bdd, changer le user et le mot de passe pour la connection.

 
```
$ git clone https://example.com
$ cd ../path/to/the/file
$ npm install
$ npm start
$ twig install

```
Vous pouvez retrouver le projet Lodecom sur gitLab ' https://GitLab.com/DWW_Castres/ECF-LODECOM '
```



