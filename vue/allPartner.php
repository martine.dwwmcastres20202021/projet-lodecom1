<!DOCTYPE html>
<html lang="fr-FR">

<!-- Auteur : GILLES nicolas,BASTARDO jeremie, GALLAND jean-brice,SOULA martine, DOSSANTOS mario-->
<!-- Modification : NIZARD alain,SOULA martine,GALLAND jean-brice,BASTARDO jeremie,SOULA cindy-->

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lodecom</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <!--Container menu des filtres -->
    <div id="app" class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card bg-light">
            <div class="row">
              <div class="col-md-12">
                <div class="card-body">
                  <form action="switch.php?allPartner=set" method="post">
                    <!--   <label for="exampleDataList" class="form-label">NOM DU CLIENT</label>-->
                    <div class="input-group rounded">
                      <span class="input-group-text border-0" id="search-addon"><i class="fas fa-search"></i></span>
                      <input type="search" class="form-control rounded" placeholder="Entrez un client_id ou un client_name..." name="client_name" id="nomClient" list="datalistOptions" />
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!--boutons actifs non actifs-->
            <div class="row justify-content-between">
              <div class="col">
                <div class="d-grid gap-2 col-6 mx-auto">
                  <form action="" class="col d-flex flex-wrap justify-content-around pb-4" method="POST">
                    <div class="form-check form-check-inline transglo">
                      <input class="form-check-input" type="radio" id="all" name="filter" value="all" checked>
                      <label class="form-check-label" for="all">Toutes</label>
                    </div>
                    <div class="form-check form-check-inline transglo">
                      <input class="form-check-input" type="radio" id="active" name="filter" value="active">
                      <label class="form-check-label" for="active">Actives</label>
                    </div>
                    <div class="form-check form-check-inline transglo">
                      <input class="form-check-input" type="radio" id="inactive" name="filter" value="inactive">
                      <label class="form-check-label" for="inactive">Inactives</label>
                    </div>
                    <div class="form-check form-check-inline transglo">
                      <input class="form-check-button filtre" type="submit" value="Filtrer">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Liste des cartes-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-xl-6" v-for="Partenaire in mesdata">
              <div class="card bg-light mb-3">
                <div class="row">
                  <div class="col-md-4">
                    <a :href=" Partenaire.url ">
                      <img :src="Partenaire.logo_url" class="card-img-top h-80" alt="...">
                    </a>
                  </div>  
                  <div class="col-md-8">
                    <div class="card-body">
                      <div class="row">
                        <h4 class="card-title pt-2 pb-2 ml-4">{{ Partenaire.client_id }}</h4>
                        <h4 class="card-text pt-2 pb-2 ml-4">{{ Partenaire.client_name }}</h4>
                      </div>
                      <p class="card-text">{{ Partenaire.short_description }}</p>
                    </div>
                  </div>
                  </div>
                  <div class="row">
                    <div class="col ml-4">
                      <form :id="'droit_s' + Partenaire.client_id" method="POST" :action="'switch.php?allPartner=set&client_id='+Partenaire.client_id">
                        <div class="custom-control custom-switch col-md-4" v-if="Partenaire.active == 'Y'">
                          <input v-on:click="confirmation('desactiver',$event, Partenaire.client_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+Partenaire.client_id" checked>
                          <input type="hidden" name="active_struct" value="N">
                          <input type="hidden" name="client_id" :value="Partenaire.client_id">
                          <label class="custom-control-label" :for="'customSwitch'+Partenaire.client_id">Active</label>
                        </div>
                        <div class="custom-control custom-switch" v-else>
                          <input v-on:click="confirmation('activer',$event, Partenaire.client_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+Partenaire.client_id">
                          <input type="hidden" name="active_struct" value="Y">
                          <input type="hidden" name="client_id" :value="Partenaire.client_id">
                          <label class="custom-control-label" :for="'customSwitch'+Partenaire.client_id">Inactive</label>
                        </div>
                      </form>
                    </div>
                    <div class="col mb-2">
                      <a :href="'switch.php?allStructure=set&client_id='+Partenaire.client_id" id="club"><button class="btn btn-success">Gérer les clubs</button></a>
                    </div>
                    <div class="col">
                      <a :href="'switch.php?infoPartner=set&client_id='+Partenaire.client_id" id="info"><button class="btn btn-success">Plus d'infos</button></a>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
      let mesdata = <?php echo json_encode($data); ?>;

      let app = new Vue({
        el: "#app",
        data: function() {
          return {
            mesdata
          };
        },
        methods: {
          confirmation: function(message, event, id) {
            result = confirm("etes vous sur de vouloir " + message);
            if (result) {
              document.forms["droit_s" + id].submit();
            } else {
              event.preventDefault();
              event.stopPropagation();
            }
          }
        }
      })
    </script>
  </body>
</html>