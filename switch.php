<!-- Auteur : Cindy -->

<?php
	// Démarrage  de session
	session_start();

	// Permet d'afficher les erreurs
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	$debug=0;
	function debug($variable){
		global $debug;
		if($debug==1){
			echo '<pre>'; print_r($variable); echo '</pre>';
		}
		if($debug==2){
			print_r($variable);
		}
	} 
	if($debug==1 OR $debug==2){ 
		echo'IndexSwitch.php - SESSION : '; print_r($_SESSION);echo'<br/>'; echo'POST : ';print_r($_POST);echo'<br/>'; echo'GET : ';print_r($_GET);echo'<br/>';
	} 

	// Include du fichier de connexion à la base de données
	include('model/config_bdd.php');

	// Et maintenant le switch qui va permettre l'accès aux différentes pages
	if (isset($_GET['allPartner']) OR isset($_POST['allPartner']) ) {
		include('controleur/displayPartner.php');
	}

	if (isset($_GET['infoPartner']) OR isset($_POST['infoPartner']) ) {
		include('controleur/displayinfoPartner.php');
	}

	if (isset($_GET['allStructure']) OR !empty($_POST['allStructure'])) {
		include('controleur/displayStructures.php');
	}

	if (isset($_GET['form']) OR !empty($_POST['form'])) {
		include('controleur/displayForm.php');
	}

	if (isset($_GET['validation']) OR !empty($_POST['validation'])) {
		include('controleur/displayForm.php');
	}
	
	if (isset($_GET['token'])) {
		include('controleur/controlToken.php');
	}